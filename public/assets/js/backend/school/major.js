define(["jquery", "bootstrap", "backend", "table", "form"], function(
  $,
  undefined,
  Backend,
  Table,
  Form
) {
  var Controller = {
    index: function() {
      // 初始化表格参数配置
      Table.api.init({
        extend: {
          index_url: "school/major/index" + location.search,
          add_url: "school/major/add",
          edit_url: "school/major/edit",
          del_url: "school/major/del",
          multi_url: "school/major/multi",
          table: "major"
        }
      });

      var table = $("#table");

      // 初始化表格
      table.bootstrapTable({
        url: $.fn.bootstrapTable.defaults.extend.index_url,
        pk: "id",
        sortName: "id",
        columns: [
          [
            { checkbox: true },
            { field: "id", title: __("Id"), visible: false, operate: false },
            { field: "name", title: __("Name") },
            {
              field: "major_level_dict",
              title: __("Major_level_dict"),
              searchList: Config.majorLevelDictList,
              operate: "FIND_IN_SET",
              formatter: Table.api.formatter.label
            },
            { field: "years", title: __("Years") },
            {
              field: "lesson_ids_text",
              title: __("Lesson_ids"),
              operate: false
            },
            {
              field: "work_station_dict",
              title: __("Work_station_dict"),
              searchList: Config.workStationDictList,
              operate: "FIND_IN_SET",
              formatter: Table.api.formatter.label
            },
            // {
            //   field: "fee",
            //   title: __("Fee"),
            //   operate: false
            // },
            { field: "level", title: __("Level") },
            {
              field: "status",
              title: __("Status"),
              searchList: { "0": __("Status 0"), "1": __("Status 1") },
              formatter: Table.api.formatter.status
            },
            {
              field: "create_by",
              title: __("Create_by"),
              operate: false,
              visible: false
            },
            {
              field: "createtime",
              title: __("Createtime"),
              operate: "RANGE",
              addclass: "datetimerange",
              formatter: Table.api.formatter.datetime
            },
            {
              field: "updatetime",
              title: __("Updatetime"),
              operate: "RANGE",
              addclass: "datetimerange",
              formatter: Table.api.formatter.datetime,
              operate: false,
              visible: false
            },
            { field: "remark", title: __("Remark"), visible: false },
            {
              field: "lessonsOperate",
              operate: false,
              title: __("课程设置"),
              table: table,
              buttons: [
                /* {
                                    name: 'detail',
                                    text: __('弹出窗口打开'),
                                    title: __('弹出窗口打开'),
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'example/bootstraptable/detail',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },*/
                /*{
                                    name: 'ajax',
                                    text: __('发送Ajax'),
                                    title: __('发送Ajax'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'example/bootstraptable/detail',
                                    confirm: '确认发送',
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg + ",返回数据：" + JSON.stringify(data));
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },*/
                {
                  name: "addtabs",
                  text: __("课程设置"),
                  title: __("课程设置"),
                  classname: "btn btn-xs btn-warning btn-addtabs",
                  icon: "fa fa-folder-o",
                  url: "school/major_lesson/index"
                }
              ],
              formatter: Table.api.formatter.buttons,
              events: Table.api.events.operate
            },
            {
              field: "operate",
              title: __("Operate"),
              table: table,
              events: Table.api.events.operate,
              formatter: Table.api.formatter.operate
            }
          ]
        ]
      });

      // 为表格绑定事件
      Table.api.bindevent(table);

      $(".btn-export").on("click", function() {
        var ids = $("#table").bootstrapTable("getSelections", null);
        var idstext = "";
        if (ids.length == 0) {
          alert("请选择专业");
        } else if (ids.length > 1) {
          alert("只能选择一个专业");
        } else {
          ids.forEach(element => {
            if (element.status == 1) {
              idstext += element.id + ",";
            } else {
              alert("所选专业是无效的");
            }
          });
          idstext += "0";
          let loadIndex = layer.load();
          var fileName = "考勤表.xls"; //设置下载时候的文件名
          var url = "major/export?ids=" + idstext;
          var xhr = new XMLHttpRequest();
          //设置响应类型为blob类型
          xhr.responseType = "blob";
          xhr.onload = function() {
            layer.close(loadIndex);
            if (this.status === 200) {
              //获取响应文件流
              var blob = this.response;
              var reader = new FileReader();
              reader.readAsDataURL(blob); // 转换为base64，可以直接放入a表情href
              reader.onload = function(e) {
                // 转换完成，创建一个a标签用于下载
                var a = document.createElement("a");
                a.download = fileName;
                a.href = e.target.result;
                $("body").append(a); // 修复firefox中无法触发click
                a.click();
                $(a).remove();
              };
            }
          };

          xhr.open("post", url, true);
          xhr.send();

          // window.location.href = "major/export?ids=" + idstext;
        }
      });
    },
    attendance: function() {
      // 初始化表格参数配置
      Table.api.init({
        extend: {
          index_url: "school/major/index" + location.search,
          add_url: "school/major/add",
          edit_url: "school/major/edit",
          del_url: "school/major/del",
          multi_url: "school/major/multi",
          table: "major"
        }
      });

      var table = $("#table");

      // 初始化表格
      table.bootstrapTable({
        url: $.fn.bootstrapTable.defaults.extend.index_url,
        pk: "id",
        sortName: "id",
        columns: [
          [
            // { checkbox: true },
            { field: "id", title: __("Id"), visible: false, operate: false },
            { field: "name", title: __("Name") },
            {
              field: "major_level_dict",
              title: __("Major_level_dict"),
              searchList: Config.majorLevelDictList,
              operate: "FIND_IN_SET",
              formatter: Table.api.formatter.label
            },
            { field: "years", title: __("Years") },
            {
              field: "lesson_ids_text",
              title: __("Lesson_ids"),
              operate: false
            },
            {
              field: "work_station_dict",
              title: __("Work_station_dict"),
              searchList: Config.workStationDictList,
              operate: "FIND_IN_SET",
              formatter: Table.api.formatter.label
            },
            // {
            //   field: "fee",
            //   title: __("Fee"),
            //   operate: false,
            //
            // },
            { field: "level", title: __("Level") },
            {
              field: "status",
              title: __("Status"),
              searchList: { "0": __("Status 0"), "1": __("Status 1") },
              formatter: Table.api.formatter.status
            },
            {
              field: "create_by",
              title: __("Create_by"),
              operate: false,
              visible: false
            },
            {
              field: "createtime",
              title: __("Createtime"),
              operate: "RANGE",
              addclass: "datetimerange",
              formatter: Table.api.formatter.datetime
            },
            {
              field: "updatetime",
              title: __("Updatetime"),
              operate: "RANGE",
              addclass: "datetimerange",
              formatter: Table.api.formatter.datetime,
              operate: false,
              visible: false
            },
            { field: "remark", title: __("Remark"), visible: false },
            {
              field: "lessonsOperate",
              operate: false,
              title: __("课程设置"),
              table: table,
              buttons: [
                {
                  name: "addtabs",
                  text: __("下载考勤表"),
                  title: __("下载考勤表"),
                  classname: "btn btn-xs btn-warning",
                  icon: "fa fa-folder-o",
                  url: "school/major/export"
                }
              ],
              formatter: Table.api.formatter.buttons,
              events: Table.api.events.operate
            }
          ]
        ]
      });

      // 为表格绑定事件
      Table.api.bindevent(table);
    },
    recyclebin: function() {
      // 初始化表格参数配置
      Table.api.init({
        extend: {
          dragsort_url: ""
        }
      });

      var table = $("#table");

      // 初始化表格
      table.bootstrapTable({
        url: "school/major/recyclebin" + location.search,
        pk: "id",
        sortName: "id",
        columns: [
          [
            { checkbox: true },
            { field: "id", title: __("Id") },
            { field: "name", title: __("Name"), align: "left" },
            {
              field: "deletetime",
              title: __("Deletetime"),
              operate: "RANGE",
              addclass: "datetimerange",
              formatter: Table.api.formatter.datetime
            },
            {
              field: "operate",
              width: "130px",
              title: __("Operate"),
              table: table,
              events: Table.api.events.operate,
              buttons: [
                {
                  name: "Restore",
                  text: __("Restore"),
                  classname: "btn btn-xs btn-info btn-ajax btn-restoreit",
                  icon: "fa fa-rotate-left",
                  url: "school/major/restore",
                  refresh: true
                },
                {
                  name: "Destroy",
                  text: __("Destroy"),
                  classname: "btn btn-xs btn-danger btn-ajax btn-destroyit",
                  icon: "fa fa-times",
                  url: "school/major/destroy",
                  refresh: true
                }
              ],
              formatter: Table.api.formatter.operate
            }
          ]
        ]
      });

      // 为表格绑定事件
      Table.api.bindevent(table);
    },
    add: function() {
      Controller.api.bindevent();
    },
    edit: function() {
      Controller.api.bindevent();
    },
    api: {
      bindevent: function() {
        Form.api.bindevent($("form[role=form]"));
      }
    }
  };
  return Controller;
});
