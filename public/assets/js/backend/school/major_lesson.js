define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var mid = Config.mId;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'school/major_lesson/index' + location.search,
                    add_url: 'school/major_lesson/add?mid=' + mid,
                    edit_url: 'school/major_lesson/edit',
                    del_url: 'school/major_lesson/del',
                    multi_url: 'school/major_lesson/multi',
                    table: 'major_lesson',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                queryParams : function (params) {
                    var filter = JSON.parse(params.filter);
                    filter.major_id = mid;
                    params.filter = JSON.stringify(filter);
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), visible:false, operate: false},
                        {field: 'major_id_text', title: __('Major_id'), operate: false},
                        {field: 'lesson_id_text', title: __('Lesson_id'), operate: false},
                        {field: 'major_level_dict', title: __('Major_level_dict'), searchList: Config.majorLevelDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label, operate: false},
                        {field: 'level', title: __('Level'), operate: false},
                        {field: 'admin_ids_text', title: __('Admin_ids'), operate: false},
                        {field: 'class_time_dict', title: __('Class_time_dict'), searchList: Config.classTimeDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label, operate: false},
                        {field: 'class_date', title: __('Class_date'), operate: false},
                        {field: 'room_dict', title: __('Room_dict'), searchList: Config.roomDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label, operate: false},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'remark', title: __('Remark'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'school/major_lesson/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'school/major_lesson/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'school/major_lesson/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();

            /*require(['moment','tempusdominus-datetimepicker'], function (moment) {
                $('#c-class_date').datetimepicker({
                    allowMultidate: true,
                    multidateSeparator:','
                }).on('dp.change', function (e) {
                    $(this, document).trigger("changed");
                });
            });*/
            // $('#c-class_date').datetimepicker({
            //             allowMultidate: true,
            //             multidateSeparator:','
            //         }).on('dp.change', function (e) {
            //             $(this, document).trigger("changed");
            //         });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
