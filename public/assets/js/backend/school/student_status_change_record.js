define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'school/student_status_change_record/index' + location.search,
                    add_url: 'school/student_status_change_record/add',
                    edit_url: 'school/student_status_change_record/edit',
                    del_url: 'school/student_status_change_record/del',
                    multi_url: 'school/student_status_change_record/multi',
                    table: 'student_status_change_record',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                queryParams : function (params) {
                    var filter = JSON.parse(params.filter);
                    var op = JSON.parse(params.op);
                    filter['s.student_number'] = filter.student_id_text;
                    delete filter.student_id_text;
                    op['s.student_number'] = op.student_id_text;
                    params.filter = JSON.stringify(filter);
                    params.op = JSON.stringify(op);
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        // {field: 'student_id', title: __('Student_id')},
                        {field: 'student_id_text', title: __('Student_id'), operate:"LIKE"},
                        {field: 'student_name', title: __('Student_name')},
                        {field: 'student_sex', title: __('Student_sex'), searchList: {"0":__('Student_sex 0'),"1":__('Student_sex 1')}, formatter: Table.api.formatter.normal},
                        {field: 'major_level_dict', title: __('Major_level_dict'), searchList: Config.majorLevelDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'student_level', title: __('Student_level')},
                        {field: 'before_status', title: __('Before_status'), searchList: {"0":__('Before_status 0'),"1":__('Before_status 1'),"2":__('Before_status 2'),"3":__('Before_status 3')}, formatter: Table.api.formatter.status},
                        {field: 'after_status', title: __('After_status'), searchList: {"0":__('After_status 0'),"1":__('After_status 1'),"2":__('After_status 2'),"3":__('After_status 3')}, formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'admin_id', title: __('Admin_id')},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            require(['selectpage'], function () {
                $('#c-student_id').selectPage({
                    eAjaxSuccess: function (data) {

                        data.list = typeof data.rows !== 'undefined' ? data.rows : (typeof data.list !== 'undefined' ? data.list : []);
                        data.totalRow = typeof data.total !== 'undefined' ? data.total : (typeof data.totalRow !== 'undefined' ? data.totalRow : data.list.length);
                        return data;

                    },
                    eSelect: function (data) {
                        $("#c-student_name").val(data.name);
                        $("#c-major_id").val(data.major_id).selectPageRefresh();
                        $("#c-student_level").val(data.level);
                        $("#c-major_level_dict").val(data.major_level_dict).selectpicker('render');
                        $("#c-sex").val(data.student_sex).selectpicker('render');
                        $("#c-before_status").val(data.status).selectpicker('render');
                    }
                });
            });
            // Controller.api.bindevent();
            Form.api.bindevent($("form[role=form]"),'','',function () {
                // $("form[role=form]").find("input,select").prop("disabled", false);
            });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});