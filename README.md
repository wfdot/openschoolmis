<h1 align="center">教学管理平台</h1>
<p align="center">
    <a href="http://www.wfcoding.com">
        <img src="https://img.shields.io/badge/Licence-Apache2.0-green.svg?style=flat" />
    </a>
    <a href="http://www.wfcoding.com">
        <img src="https://img.shields.io/badge/Edition-0.1-blue.svg" />
    </a>
     <a href="https://gitee.com/liupras/openschoolmis/repository/archive/master.zip">
        <img src="https://img.shields.io/badge/download-24.4m-red.svg" />
    </a>
    </p>
<p align="center">    
    <b>如果对您有帮助，麻烦点右上角 "Star" 支持一下，谢谢！</b>
</p>

#### 介绍
教学管理平台主要服务于国内的高中和大学，包括档案管理（学校、教师、学生、家长）、学籍管理、课程表、成绩管理 、学费管理，专业管理等功能，打通学校、教师、学生/家长的信息流，提升学校教育教学水平和管理能力。

#### 软件架构
使用PHP、MySQL开发，基于fastadmin框架。

### :tw-1f427: QQ交流群
 QQ开发群：994690164<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=97f23179a3ea43dd17ed827678dfad03aab5bb9b0275a1195495a749db5ccfee"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="开发群" title="开发群"></a>

### 页面展示
![demo](/readme/image/demo.jpg "demo.jpg")

### 参与开发

请提交 [教学管理平台](https://gitee.com/liupras/openschoolmis/pulls)。
