/*
Navicat SQL Server Data Transfer

Source Server         : 123.56.177.192,1433
Source Server Version : 110000
Source Host           : 123.56.177.192,1433:1433
Source Database       : YJXErp
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 110000
File Encoding         : 65001

Date: 2020-02-09 19:42:02
*/


-- ----------------------------
-- Table structure for Module
-- ----------------------------
DROP TABLE [dbo].[Module]
GO
CREATE TABLE [dbo].[Module] (
[Id] uniqueidentifier NOT NULL DEFAULT (newid()) ,
[CascadeId] varchar(50) NOT NULL ,
[Name] nvarchar(50) NOT NULL ,
[Url] varchar(256) NULL ,
[IsLeaf] bit NOT NULL DEFAULT ((0)) ,
[IconName] varchar(256) NULL ,
[Status] bit NOT NULL DEFAULT ((1)) ,
[ParentId] uniqueidentifier NULL ,
[ParentName] nvarchar(50) NULL ,
[CreateTime] datetime NOT NULL DEFAULT (getdate()) ,
[CreatorId] uniqueidentifier NOT NULL ,
[SortNo] int NOT NULL DEFAULT ((0)) ,
[Kind] int NOT NULL DEFAULT ((1)) 
)


GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'Module', 
'COLUMN', N'Id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'序号'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'Module'
, @level2type = 'COLUMN', @level2name = N'Id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'序号'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'Module'
, @level2type = 'COLUMN', @level2name = N'Id'
GO

-- ----------------------------
-- Records of Module
-- ----------------------------
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'0.33', N'产品管理', null, N'1', N'fa-list-ul', N'1', null, N'根节点', N'2016-12-24 17:00:25.437', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'100', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'7F5B7EEA-0CAE-4070-BE52-020F4EE53009', N'0.37.5', N'已完成加工单查询', N'/Worksheet/OksResult', N'1', null, N'1', N'94C85E69-A001-4034-8655-E4BDAB482DC1', N'生产管理', N'2017-03-11 18:42:00.170', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'707', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'24BF4391-338F-4CB1-92B0-04E90674A1A2', N'0.45.5', N'已完成订单', N'/CustomerService/OrderFinish', N'1', null, N'1', N'A9EB07FD-FCB8-4997-B7D1-70F8AC013C1E', N'售后管理', N'2017-06-04 12:00:43.977', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1105', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'5C541F45-619D-4C23-AF36-053645F5FFBB', N'0.50.41', N'调库', N'/StorageTable/OutStorage', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-02-23 17:42:50.680', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1204', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C9EDEAEC-332F-4173-ABC3-0574897F6461', N'0.39', N'信息查询', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2016-12-27 12:16:53.207', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1222', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'60B12D47-76CC-449A-8B3E-06B594A81C0D', N'0.46.15', N'应收账款查询', N'/FinanceCost/YingshouQuery', N'1', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务管理', N'2017-03-15 11:54:11.157', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'611', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3D74D212-CCFA-45A9-960B-07166B4060F3', N'0.44.15', N'添加公告', N'/Announcement/Add', N'1', null, N'1', N'4F10D7A0-4A4B-4CC5-8362-1DEF247A5634', N'行政', N'2017-01-19 16:35:50.263', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1701', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'2DC0B9DA-44B8-47D8-B4CE-073DD51F2BE8', N'0.46.11', N'设备费用维护', N'/FinanceCost/TechType', N'1', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务管理', N'2017-02-18 17:01:44.910', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'615', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'E2AA8006-0821-4FBA-8D32-08ABBE804DE8', N'0.47.15', N'产品价格', N'/Product/ChangeList', N'1', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-05-14 10:32:40.203', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'508', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3D4DE7D0-358A-4AA2-A641-0A735D624571', N'0.41.9', N'产品管理', N'/SalerDirector/ProList', N'1', null, N'1', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-01-31 13:42:57.837', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'351', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C89DD92F-DD48-483D-843B-0ABD19125D40', N'0.35.2', N'核算查询', N'/FinanceCost/QueryOrder', N'1', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务', N'2016-12-26 15:13:47.750', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'607', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'DA7452DA-DCD2-4EC0-8A7A-0CA7242A2E3D', N'0.46.14', N'开票查询', N'/FinanceCost/ReceivableQuery', N'1', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务管理', N'2017-03-15 11:26:19.600', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'610', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'33F8985E-BDE6-4A9B-99AE-1088FFDFF77E', N'0.47.9', N'采购查询', N'/Purchase/PurQuery', N'1', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-02-11 17:21:18.273', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'520', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'4C6CD5F0-4520-46E6-8043-136BF64D8FBD', N'0.36.8', N'客户信息', N'/Customer/List', N'1', null, N'1', N'C9EDEAEC-332F-4173-ABC3-0574897F6461', N'信息查询', N'2017-02-17 12:26:56.840', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'91', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'0D4CAEBD-E226-467F-B537-172C00869F49', N'0.41.8', N'查询订单', N'/SalerDirector/Order', N'1', null, N'1', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-01-31 13:36:10.913', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'356', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'5C1627A6-CA23-4592-87B4-17944862BFAE', N'0.40.5', N'检验查询', N'/Examine/Check', N'1', null, N'1', N'F218EB85-9BD9-4478-B730-A40C9B4035EF', N'检验管理', N'2017-03-16 14:58:07.587', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'905', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3C9682AC-FCE2-4C36-882B-1A6B8230B6FD', N'0.41.16', N'审核最终价', N'/SalerDirector/CheckPriceList', N'0', null, N'1', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-06-16 14:42:40.147', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'355', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C9FB13EB-66CB-457A-90DD-1CA6CBB68F78', N'0.37.4', N'加工价格维护', N'/Worksheet/GradeMaintenanceJg', N'1', null, N'1', N'94C85E69-A001-4034-8655-E4BDAB482DC1', N'生产管理', N'2017-02-18 17:00:57.237', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'710', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'09874A12-CA3C-441F-90C1-1DC24DEF6CA5', N'0.50.46', N'产品库盘点', N'/StorageTable/TakeProduct', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2019-12-21 14:00:05.627', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1211', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'9672CACD-00F9-4078-A234-1E5B469542FD', N'0.44.20', N'学习园地类型', N'/LearningType/Index', N'1', null, N'1', N'4F10D7A0-4A4B-4CC5-8362-1DEF247A5634', N'行政', N'2017-01-19 16:36:35.113', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1706', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'985DA1A7-4184-4985-BF49-1F2D4D688657', N'0.13.13.1', N'产品查询-编辑', N'/Product/Edit', N'1', null, N'1', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2019-12-19 18:56:31.863', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'10101', N'2')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'E09FC624-D097-42F8-8BE7-240473CA8664', N'0.50.34', N'原料出库', N'/StorageTable/OutMaterial', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-12 16:24:32.230', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1216', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'82211F89-4FFF-44FC-85C4-25CACD5BFA8F', N'0.50.25', N'入库', N'/StorageTable/PutInProduct', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-02 18:41:50.323', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1205', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3A27D5BE-BBD9-4B28-B771-2735A48C8DAA', N'0.50.32', N'出库记录', N'/StorageTable/OutStorageRecord', N'1', null, N'1', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-12 16:23:56.967', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1208', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'BE19780A-0425-4977-893D-2BCF3C43B2F4', N'0.50.38', N'原料入库记录', N'/StorageTable/PutInMaterialRecord', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-12 17:00:00.357', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1217', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C525AF18-3B5F-4B77-B769-2DEABADCD7C5', N'0.52.6', N'辅料查询', N'/Material/Accessory', N'1', null, N'0', N'8C4AB4AD-9807-4713-B13A-6E27F40BA9E2', N'原料管理', N'2017-04-03 11:12:02.447', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'584', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'1EBF41F1-E689-4378-8F33-2FCC27D17213', N'0.50.45', N'库房设置', N'/Storage/Index', N'1', null, N'1', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2019-12-21 13:17:30.813', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'16', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'D31F0040-F6EE-48BF-BF4B-3473BEF859D7', N'0.21', N'订单管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2016-12-16 18:35:35.257', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'300', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'5477CE9D-2C4D-40F6-8327-3AF7BC06E108', N'0.16.16', N'客户添加', N'/Customer/Add', N'1', null, N'1', N'1249C5A0-AD1B-41CF-A17B-C7E4E734AFFC', N'客户管理', N'2016-12-26 14:43:01.650', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'202', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'E35D2FDE-00EE-47D6-A4A7-3C441681E465', N'0.43', N'运输管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2017-01-19 12:26:29.623', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1000', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'B78F613F-F93D-4FE4-B9C0-3D71F9E63241', N'0.21.8', N'已完成订单', N'/Order/FinishList', N'1', null, N'1', N'D31F0040-F6EE-48BF-BF4B-3473BEF859D7', N'订单管理', N'2017-05-03 11:58:02.593', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'316', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'98063E30-29C3-4E1D-8DF3-3EC62DF65AA6', N'0.30', N'系统安全', null, N'1', N'fa-list-ul', N'1', null, N'根节点', N'2016-12-21 20:06:42.970', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'2900', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'ACB11BDC-E3E1-44ED-B9C0-3F1435F04241', N'0.13.12', N'产品查询-价格', N'/Product/Index_Price', N'1', N'', N'1', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2016-12-22 11:14:13.273', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'101', N'2')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'AFB22BDC-E3E1-44ED-B9C0-3F1435F04241', N'0.13.13', N'产品查询', N'/Product/Index', N'1', null, N'1', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2016-12-22 11:14:13.273', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'101', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'B26670BD-354F-4FD6-8712-4684F01B8C1C', N'0.45.3', N'订单查询', N'/CustomerService/Orders', N'1', null, N'1', N'A9EB07FD-FCB8-4997-B7D1-70F8AC013C1E', N'售后管理', N'2017-05-05 18:39:35.467', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1104', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'88D89887-470D-4F81-A865-47EE49467A0D', N'0.43.7', N'已取消发货单', N'/Transport/HasCancelInvoice', N'0', null, N'1', N'E35D2FDE-00EE-47D6-A4A7-3C441681E465', N'运输管理', N'2017-06-15 16:31:47.610', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1004', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'60E0AB75-1FCC-4B3E-9EE1-4900F216AF2B', N'0.47.11', N'生产计划分配', N'/Purchase/PurchaseQuery', N'1', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-03-11 14:42:08.937', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'515', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'D562EE81-5513-4A99-8A63-4C6E58828B11', N'0.44.16', N'查询公告', N'/Announcement/Index', N'1', null, N'1', N'4F10D7A0-4A4B-4CC5-8362-1DEF247A5634', N'行政', N'2017-01-19 16:35:58.390', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1702', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C7230039-F62D-4974-8459-4D029D313A9E', N'0.47', N'采购管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2017-01-19 16:47:23.767', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'500', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'9E6263A1-4E6F-4B5A-806D-50B2062803BC', N'0.41.12', N'已完成订单', N'/SalerDirector/FinishList', N'1', null, N'1', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-03-19 11:29:35.750', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'357', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'1AF663A5-ECB9-45F1-AD3F-50C7DA19D5D6', N'0.47.16', N'已对账', N'/Purchase/CompleteReconc', N'0', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-06-03 12:18:58.713', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'525', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'40DA677C-B8A3-4AE5-88FD-50FB38BB3F01', N'0.50.33', N'原料入库', N'/StorageTable/PutInMaterials', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-12 16:24:23.657', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1215', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'8AD96171-8E4F-4896-8AF4-5271580BC786', N'0.51', N'供应商管理', null, N'0', N'fa-list-ul', N'0', null, N'根节点', N'2017-02-20 16:36:36.073', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'550', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3A73FAFD-35D6-4C09-804A-549C276EB957', N'0.43.5', N'派车单查询', N'/Transport/SendCarQuery', N'1', null, N'1', N'E35D2FDE-00EE-47D6-A4A7-3C441681E465', N'运输管理', N'2017-05-16 12:26:39.287', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1007', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'2DE2AC44-FFE5-432D-BA4D-55BF01B5C555', N'0.33.15', N'提交成本申请', N'../Order/ApplyAdd', N'1', null, N'0', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2017-03-07 11:14:33.323', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'104', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'FACCDF59-83FE-415B-89C6-598AA9759D92', N'0.47.19', N'财务应付', N'/Purchase/YingFuQuery', N'0', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-06-20 14:05:09.487', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'527', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3E8FE7B9-9798-4B46-B820-5A72852F6D28', N'0.46.13', N'加工费用维护', N'/FinanceCost/GradeMaintenanceTwo', N'1', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务管理', N'2017-02-18 17:02:17.153', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'625', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'D13BEFF6-B2EE-4E69-B8D9-6108E9BAA46F', N'0.50.40', N'调库记录', N'/StorageTable/InventoryChangeList', N'1', null, N'1', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-04-11 17:02:54.517', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1222', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'A8407249-9763-4FBB-BB1D-620FCE118B20', N'0.33.24', N'确认下单', N'/Order/ConfirmOrders', N'0', null, N'0', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2017-06-17 11:00:02.170', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'107', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'9E67F191-F822-4515-9675-6253B5A684B9', N'0.45.8', N'财务应收', N'/CustomerService/YingshouQuery', N'0', null, N'1', N'A9EB07FD-FCB8-4997-B7D1-70F8AC013C1E', N'售后管理', N'2017-06-20 14:59:19.800', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1108', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'9459BB23-F8CB-4786-98AF-626CEF000168', N'0.50', N'库房管理', null, N'1', N'fa-list-ul', N'1', null, N'根节点', N'2017-01-20 18:48:56.970', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1200', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C468D011-0788-49A9-95EF-630D4EF35B42', N'0.47.14', N'打印采购单', N'/Purchase/PurchasePrint', N'1', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-04-06 12:09:38.240', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'523', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'8726E32F-023E-4D6D-9776-65835704274E', N'0.30.1', N'系统日志', N'/LogInLog/Index', N'1', null, N'1', N'98063E30-29C3-4E1D-8DF3-3EC62DF65AA6', N'系统安全', N'2017-03-16 14:40:12.270', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'5', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'231EB732-D1FE-4E8A-9646-671F8AE77A90', N'0.29', N'基本设置', null, N'1', N'fa-list-ul', N'1', null, N'根节点', N'2016-12-21 20:06:21.430', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'3000', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'8C4AB4AD-9807-4713-B13A-6E27F40BA9E2', N'0.52', N'原料管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2017-02-20 19:38:30.243', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'580', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'A9EB07FD-FCB8-4997-B7D1-70F8AC013C1E', N'0.45', N'售后管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2017-01-19 16:37:08.560', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1100', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3C00A976-2B09-4AC8-BDAF-748EA2746381', N'0.41.5', N'查询成本申请', N'/SalerDirector/Applylist', N'1', null, N'0', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-01-31 12:23:05.580', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'353', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'9B3D2C6B-9537-44E7-BF7B-751BBE5B82C2', N'0.45.7', N'打印发货单', N'/Order/InvoiceQuery', N'1', null, N'1', N'A9EB07FD-FCB8-4997-B7D1-70F8AC013C1E', N'售后管理', N'2017-06-04 17:37:53.987', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1104', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'DBAA5299-2937-4B88-AE99-75AF36345671', N'0.51.1', N'供应商查询', N'/Supplier/SupplierList', N'0', null, N'1', N'8AD96171-8E4F-4896-8AF4-5271580BC786', N'供应商管理', N'2017-02-20 16:40:34.500', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'551', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'DD8E7CAF-32F3-40E8-B337-78FDB4B94566', N'0.40.4', N'已完成订单', N'/Examine/CheckFinish', N'1', null, N'1', N'F218EB85-9BD9-4478-B730-A40C9B4035EF', N'检验管理', N'2017-03-11 18:53:52.607', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'906', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'0.46', N'财务管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2017-01-19 16:47:14.783', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'600', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'CB7B0524-5365-4D40-B13F-7D6B86DE1462', N'0.48.21', N'查询学习资料', N'/Learning/LearnIndex', N'1', null, N'1', N'4F10D7A0-4A4B-4CC5-8362-1DEF247A5634', N'行政管理', N'2017-01-22 12:26:03.533', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1704', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3EDC4D3C-4E30-4834-968A-7E6A814A2222', N'0.44.18', N'添加学习资料', N'/Learning/Add', N'1', null, N'1', N'4F10D7A0-4A4B-4CC5-8362-1DEF247A5634', N'行政', N'2017-01-19 16:36:15.150', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1705', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'01BB1B40-CCF5-4118-A80F-80A080344E78', N'0.21.5', N'订单查询', N'/Order/OrderList', N'1', null, N'1', N'D31F0040-F6EE-48BF-BF4B-3473BEF859D7', N'订单管理', N'2017-03-07 11:17:27.113', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'315', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'D3F1F855-78F2-432A-820E-82D34FEE37FA', N'0.51.2', N'供应商添加', N'/Supplier/SupplierAdd', N'1', null, N'1', N'8AD96171-8E4F-4896-8AF4-5271580BC786', N'供应商管理', N'2017-02-20 16:40:54.597', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'552', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'7B3B2AF0-A0FC-42B0-91A8-85CAE42B40B6', N'0.46.17', N'应收完成', N'/FinanceCost/CompleteYingshou', N'0', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务管理', N'2017-06-03 12:16:00.240', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'613', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'286A60AD-BAD9-433C-BE7F-875C6999FD4E', N'0.24.11', N'设备维护', N'/Technology/Equipment', N'0', null, N'1', N'294B6A20-6F59-457D-8F5B-D371434A32ED', N'工艺管理', N'2017-02-08 16:16:22.277', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'404', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'2C9BE114-8DD3-467F-8458-8F99A0E4DDBB', N'0.33.16', N'查询成本申请', N'../Order/ApplyList', N'1', null, N'0', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2017-03-07 11:14:55.737', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'103', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'970B54DC-4336-4D7F-89E5-91445012AA2C', N'0.37.3', N'查询加工申请', N'/Worksheet/Check', N'1', null, N'1', N'94C85E69-A001-4034-8655-E4BDAB482DC1', N'生产管理', N'2017-01-20 18:09:53.270', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'705', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'B7505DFE-4FA2-4A6D-8B8B-970B0BD9965B', N'0.50.42', N'转库存申请', N'/StorageTable/InventoryChangeApplyList', N'0', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-07-16 19:12:41.213', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1221', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'8F108957-49CF-49C5-8916-9715F5D625FD', N'0.41.10', N'客户管理', N'/SalerDirector/CusList', N'0', null, N'0', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-02-03 15:58:12.423', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'352', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'B254025F-7E40-45E0-8E7B-9B6A0ACC80F7', N'0.52.8', N'主料添加', N'/Material/MaterialAdd', N'1', null, N'1', N'8C4AB4AD-9807-4713-B13A-6E27F40BA9E2', N'原料管理', N'2017-04-03 11:12:29.337', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'582', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'4524A41E-6269-4015-A17B-9E8FF55B2B7E', N'0.24.10', N'工艺核算查询', N'/Technology/Query', N'1', null, N'1', N'294B6A20-6F59-457D-8F5B-D371434A32ED', N'工艺管理', N'2017-01-11 10:56:44.237', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'402', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'F218EB85-9BD9-4478-B730-A40C9B4035EF', N'0.40', N'检验管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2017-01-06 16:14:20.327', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'900', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'30F212D3-75AA-450E-B76E-A6AE348CF909', N'0.47.12', N'已完成采购查询', N'/Purchase/PurQueryOk', N'1', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-03-11 16:55:28.717', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'522', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'D204FBD0-5918-431E-B44C-A6C2450C0161', N'0.38.1', N'采购查询', N'/PurchasePerson/PurchaseQuery', N'1', null, N'1', N'F99B2471-FC89-436B-BA7B-F5D997FFAA37', N'采购负责人', N'2017-01-03 15:32:37.687', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'810', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'8763C3F4-7DD3-4D03-A36B-A7F5679DED94', N'0.43.2', N'运输审核', N'/Transport/TransportExamine', N'1', null, N'1', N'E35D2FDE-00EE-47D6-A4A7-3C441681E465', N'运输管理', N'2017-03-08 15:38:33.887', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1005', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'58C5489C-DF01-43C6-AABC-A90FDEFAC7E5', N'0.45.4', N'提交订单', N'/Customer/SaleCusList', N'1', null, N'1', N'A9EB07FD-FCB8-4997-B7D1-70F8AC013C1E', N'售后管理', N'2017-05-14 16:16:34.723', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1102', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'1F49C8AA-EC9B-4C6C-93F9-A9D87BE06591', N'0.31.2', N'核算查询', N'/Purchase/QueryOrder', N'1', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购', N'2016-12-22 10:46:16.383', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'511', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'5DAB9B55-BE40-4443-83D2-A9E5CBFBB377', N'0.21.9', N'已取消订单', N'/Order/CancelList', N'1', null, N'1', N'D31F0040-F6EE-48BF-BF4B-3473BEF859D7', N'订单管理', N'2017-05-03 11:58:12.510', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'317', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'9B6F0D6C-2A8C-4609-93F0-AEF9B4DE154C', N'0.45.1', N'对帐查询', N'/CustomerService/InvoiceQuery', N'1', null, N'1', N'A9EB07FD-FCB8-4997-B7D1-70F8AC013C1E', N'售后管理', N'2017-01-19 16:39:24.987', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1105', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C9682AD0-C485-4FA7-8292-AF8DFF5AB508', N'0.41.15', N'确认指导价', N'/SalerDirector/GuidePrice', N'0', null, N'1', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-06-16 14:29:17.673', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'354', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'BF16BAB4-6FE8-4AE7-8215-AFB8781583E6', N'0.47.13', N'采购对账', N'/Purchase/Reconciliation', N'1', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-03-22 11:26:30.747', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'524', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'AFBF23F4-B385-43C0-8246-B36A299A08B9', N'0.13.14', N'产品添加', N'/Product/Add', N'1', null, N'1', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2016-12-22 11:14:21.113', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'102', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'9F0C9202-3F31-4DB8-A735-B561A0BBC95C', N'0.24.7', N'工艺核算', N'/Technology/Index', N'1', null, N'1', N'294B6A20-6F59-457D-8F5B-D371434A32ED', N'工艺管理', N'2016-12-22 11:13:04.930', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'401', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'E72934F0-7D08-4BC2-96B0-B7831FA26B00', N'0.50.26', N'出库', N'/StorageTable/OutProduct', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-02 18:42:04.060', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1206', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'090B77B9-9E78-443C-B2A4-B85806BA0DF4', N'0.46.5', N'价格核算', N'/FinanceCost/CheckOrder', N'1', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务管理', N'2017-02-07 11:19:20.487', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'606', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'E1E9D052-2333-4F4A-8A1C-BBD8929996CD', N'0.16.17', N'客户查询', N'/Customer/Index', N'1', null, N'1', N'1249C5A0-AD1B-41CF-A17B-C7E4E734AFFC', N'客户管理', N'2016-12-26 14:43:08.630', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'201', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'94EFD929-6B49-4238-B713-BFAF7BB33887', N'0.44.17', N'公告类型', N'/AnnouncementType/Index', N'1', null, N'1', N'4F10D7A0-4A4B-4CC5-8362-1DEF247A5634', N'行政', N'2017-01-19 16:36:06.317', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1703', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'E1B4FD68-2EF3-4034-AD26-C139A05010FB', N'0.52.7', N'主料查询', N'/Material/Index', N'1', null, N'1', N'8C4AB4AD-9807-4713-B13A-6E27F40BA9E2', N'原料管理', N'2017-04-03 11:12:18.540', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'581', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'A1959EEB-63E8-478C-9885-C2F398C4D49B', N'0.41.17', N'产品审核', N'/SalerDirector/ProCheckList', N'0', null, N'1', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-07-13 18:23:44.877', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'350', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'E682B6C6-27C1-4346-B011-C325F582F5BA', N'0.47.18', N'已完成计划分配', N'/Purchase/PurchaseIsOkQuery', N'0', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-06-07 18:31:11.990', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'516', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'6656A34A-DFE4-4B82-979D-C47268B769F6', N'0.50.39', N'原料出库记录', N'/StorageTable/OutMaterialRecord', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-12 17:00:07.737', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1218', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'0FA3A0A1-1D69-43AF-AC6E-C4DBF237F766', N'0.37.2', N'加工单查询', N'/Worksheet/Query', N'1', null, N'1', N'94C85E69-A001-4034-8655-E4BDAB482DC1', N'生产管理', N'2016-12-28 11:09:54.667', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'706', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'127F58B6-FB6A-4E03-86A7-C5FCA8B832FB', N'0.50.34', N'产品库存-编辑', N'/StorageTable/InventoryProductEdit', N'1', N'', N'1', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-12 16:24:49.867', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1210', N'2')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'F67F58B6-FB6A-4E03-86A7-C5FCA8B832FB', N'0.50.35', N'产品库存', N'/StorageTable/InventoryProduct', N'1', null, N'1', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-12 16:24:49.867', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1209', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'1249C5A0-AD1B-41CF-A17B-C7E4E734AFFC', N'0.16', N'客户管理', null, N'1', N'fa-list-ul', N'1', null, N'根节点', N'2016-12-16 18:30:38.460', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'200', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'4E3F1565-3C0E-4258-93DA-CA4FFC928E4F', N'0.50.46', N'原料库盘点', N'/StorageTable/TakeMaterial', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2019-12-21 16:41:52.870', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1219', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'2E22CA9B-C9FD-42C7-9BCA-CCF2416CEFED', N'0.36.10', N'订单查询', N'/Order/OrderSel', N'1', null, N'0', N'C9EDEAEC-332F-4173-ABC3-0574897F6461', N'信息查询', N'2017-02-17 12:27:33.517', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'95', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C0CC98D0-1CDB-4313-881F-CD95410F5C0B', N'0.24.9', N'工艺未维护', N'/Technology/Service', N'1', null, N'1', N'294B6A20-6F59-457D-8F5B-D371434A32ED', N'工艺管理', N'2017-01-11 10:14:29.940', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'403', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'3C97AC0D-21C4-48CA-A3C0-CE89BEB36E33', N'0.50.11', N'入库申请', N'/StorageTable/PutInStorage', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-02-23 17:42:40.127', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1203', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'16C99B59-5D00-44F1-ABBD-CED16EFDCCA3', N'0.1.3', N'分类编码管理', N'/Category/Index', N'1', N'', N'1', N'231EB732-D1FE-4E8A-9646-671F8AE77A90', N'基本设置', N'2016-12-02 17:56:15.363', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'0', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'A6C99B59-5D00-44F1-ABBD-CED16EFDCCA3', N'0.1.3', N'用户管理', N'/User/Index', N'1', N'', N'1', N'231EB732-D1FE-4E8A-9646-671F8AE77A90', N'基本设置', N'2016-12-02 17:56:15.363', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'0', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'DC44576E-7DE6-4DD0-80B5-D0F6CD2BEFB4', N'0.41.11', N'已取消订单', N'/SalerDirector/CancelList', N'1', null, N'1', N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'销售管理', N'2017-03-19 11:29:06.803', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'358', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'294B6A20-6F59-457D-8F5B-D371434A32ED', N'0.24', N'工艺管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2016-12-16 18:36:07.923', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'400', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'8DE8E2AB-DFA4-4A44-AF7D-D3C24133DA87', N'0.47.20', N'添加运输单', N'/Purchase/AddSendCard', N'0', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-06-21 15:17:50.387', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'529', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'77DC974A-7284-4F89-A3EF-D7464957DB2D', N'0.46.12', N'检验费用维护', N'/FinanceCost/GradeMaintenanceService', N'1', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务管理', N'2017-02-18 17:01:57.943', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'620', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'41A10AFC-D87A-4CEE-800C-DA198E2582AD', N'0.36.9', N'产品信息', N'/Product/ListPro', N'1', null, N'1', N'C9EDEAEC-332F-4173-ABC3-0574897F6461', N'信息查询', N'2017-02-17 12:27:04.957', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'92', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'7154242C-BBE9-4A1C-BB2E-E173C0D710E3', N'0.43.6', N'快递单查询', N'/Transport/ExpressOrderQuery', N'1', null, N'1', N'E35D2FDE-00EE-47D6-A4A7-3C441681E465', N'运输管理', N'2017-05-16 12:26:47.813', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1008', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'FD83612C-4D6B-4511-B6E7-E189382644CE', N'0.50.18', N'原料库存', N'/StorageTable/InventoryMaterial', N'1', null, N'0', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-02 18:31:26.133', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1220', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'94C85E69-A001-4034-8655-E4BDAB482DC1', N'0.37', N'生产管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2016-12-28 11:08:36.197', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'700', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'72A02B31-5457-48A5-A673-EA0494DE09EF', N'0.41', N'销售管理', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2017-01-13 16:02:58.617', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'350', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'11626EC7-6F87-4168-A2B9-F32C6DBD842C', N'0.45.2', N'寄票查询', N'/CustomerService/SendTicketQuery', N'1', null, N'1', N'A9EB07FD-FCB8-4997-B7D1-70F8AC013C1E', N'售后管理', N'2017-03-12 10:53:11.573', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1106', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'9D974F30-D598-46BD-8EBB-F38DCECC60DB', N'0.43.1', N'发货单查询', N'/Transport/InvoiceQuery', N'1', null, N'1', N'E35D2FDE-00EE-47D6-A4A7-3C441681E465', N'运输管理', N'2017-01-19 12:28:50.127', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1003', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'F99B2471-FC89-436B-BA7B-F5D997FFAA37', N'0.49', N'采购审核', null, N'1', N'fa-list-ul', N'0', null, N'根节点', N'2017-01-20 18:12:20.643', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'800', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'1BADB5B4-9BC6-4912-859D-F7CFC30C4420', N'0.47.10', N'采购合并', N'/Purchase/PurchaseCompose', N'1', null, N'1', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购管理', N'2017-02-20 14:15:07.097', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'519', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'7734A4C7-9D6F-4001-A8D2-FAD2CAED9215', N'0.50.31', N'入库记录', N'/StorageTable/PutInStorageRecord', N'1', null, N'1', N'9459BB23-F8CB-4786-98AF-626CEF000168', N'库房管理', N'2017-03-12 16:23:48.183', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'1207', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'C7B9B58B-7905-4576-94B2-FBC952DABE12', N'0.31.1', N'原料成本核算', N'/Purchase/Check ', N'1', null, N'0', N'C7230039-F62D-4974-8459-4D029D313A9E', N'采购', N'2016-12-21 20:09:13.020', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'510', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'5FA05BCB-84EF-4480-97E4-FCE23AFEE426', N'0.52.9', N'辅料添加', N'/Material/AccessoryAdd', N'1', null, N'1', N'8C4AB4AD-9807-4713-B13A-6E27F40BA9E2', N'原料管理', N'2017-04-03 11:14:35.657', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'585', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'579F4AA9-6908-4078-9742-FE6456B8015F', N'0.33.21', N'提交最终价', N'/Order/ApplyPrice', N'0', null, N'0', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2017-06-16 14:27:34.230', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'106', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'FF6B6480-ADDA-47DE-887C-FEA2C8902B37', N'0.46.16', N'应付账款查询', N'/FinanceCost/YingFuQuery', N'1', null, N'1', N'E28A8899-2371-4A1F-B2D3-7CEFF574405D', N'财务管理', N'2017-03-15 11:55:23.277', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'612', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'17156EFD-0647-4570-82C0-FED3B20D8D84', N'0.52.8', N'库存查询', N'/Product/InventoryList', N'1', null, N'0', N'8C393139-6A48-4D7D-89D3-01C6659F767F', N'产品管理', N'2018-07-25 00:00:00.000', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'112', N'1')
GO
GO
INSERT INTO [dbo].[Module] ([Id], [CascadeId], [Name], [Url], [IsLeaf], [IconName], [Status], [ParentId], [ParentName], [CreateTime], [CreatorId], [SortNo], [Kind]) VALUES (N'17156EFD-0647-4570-82C0-FED3B20D8D85', N'0.24.14', N'工艺已维护', N'/Technology/ServiceBack', N'1', null, N'1', N'294B6A20-6F59-457D-8F5B-D371434A32ED', N'工艺管理', N'2017-05-10 15:45:59.737', N'3A95E392-07D4-4AF3-B30D-140CA93340F5', N'405', N'1')
GO
GO

-- ----------------------------
-- Indexes structure for table Module
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Module
-- ----------------------------
ALTER TABLE [dbo].[Module] ADD PRIMARY KEY ([Id])
GO
