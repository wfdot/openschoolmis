<?php

namespace app\admin\model\school;

use think\Model;
use think\Config;
use traits\model\SoftDelete;

class Studentbyzg extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'student';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'work_station_dict_text',
        'exametime_text',
        'major_level_dict_text',
        'student_type_dict_text',
        'student_sex_text',
        'student_political_status_dict_text',
        'status_text',
        'student_last_level_dict_text',
        'student_last_type_dict_text',
        'fee_status_text',
        'cj_status_text',
        'lw_status_text',
        'by_status_text',
        'major_id_text',
        'major_id_lev',
        'jy_status_text'
    ];
    

    
    public function getWorkStationDictList()
    {
        return Config::get("site.work_station");
    }

    public function getMajorLevelDictList()
    {
        return Config::get("site.major_level");
    }

    public function getStudentTypeDictList()
    {
        return Config::get("site.student_type");
    }

    public function getStudentSexList()
    {
        return ['0' => __('Student_sex 0'), '1' => __('Student_sex 1')];
    }

    public function getStudentPoliticalStatusDictList()
    {
        return Config::get("site.student_political_status");
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1'), '2' => __('Status 2'), '3' => __('Status 3')];
    }

    public function getStudentLastLevelDictList()
    {
        return Config::get("site.student_last_level");
    }

    public function getStudentLastTypeDictList()
    {
        return Config::get("site.student_last_type");
    }

    public function getFeeStatusList()
    {
        return ['0' => __('Fee_status 0'), '1' => __('Fee_status 1')];
    }

    public function getCjStatusList()
    {
        return ['0' => __('Cj_status 0'), '1' => __('Cj_status 1')];
    }

    public function getLwStatusList()
    {
        return ['0' => __('Lw_status 0'), '1' => __('Lw_status 1')];
    }

    public function getByStatusList()
    {
        return ['0' => __('By_status 0'), '1' => __('By_status 1')];
    }

    public function getJyStatusList()
    {
        return ['0' => __('Jy_status 0'), '1' => __('Jy_status 1')];
    }


    public function getWorkStationDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['work_station_dict']) ? $data['work_station_dict'] : '');
        $list = $this->getWorkStationDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getExametimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['exametime']) ? $data['exametime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getMajorLevelDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['major_level_dict']) ? $data['major_level_dict'] : '');
        $list = $this->getMajorLevelDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStudentTypeDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['student_type_dict']) ? $data['student_type_dict'] : '');
        $list = $this->getStudentTypeDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStudentSexTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['student_sex']) ? $data['student_sex'] : '');
        $list = $this->getStudentSexList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStudentPoliticalStatusDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['student_political_status_dict']) ? $data['student_political_status_dict'] : '');
        $list = $this->getStudentPoliticalStatusDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStudentLastLevelDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['student_last_level_dict']) ? $data['student_last_level_dict'] : '');
        $list = $this->getStudentLastLevelDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStudentLastTypeDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['student_last_type_dict']) ? $data['student_last_type_dict'] : '');
        $list = $this->getStudentLastTypeDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getFeeStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['fee_status']) ? $data['fee_status'] : '');
        $list = $this->getFeeStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCjStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['cj_status']) ? $data['cj_status'] : '');
        $list = $this->getCjStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getLwStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['lw_status']) ? $data['lw_status'] : '');
        $list = $this->getLwStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getByStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['by_status']) ? $data['by_status'] : '');
        $list = $this->getByStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getJyStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['jy_status']) ? $data['jy_status'] : '');
        $list = $this->getJyStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setExametimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    public function getMajorIdTextAttr($value, $data)
    {
        $major = (new Major())->field("name")->where("id", "=", $data['major_id'])->find();
        return $major['name'];
    }

    public function getMajorIdLevAttr($value, $data)
    {
        $major = (new Major())->field("level")->where("id", "=", $data['major_id'])->find();
        return $major['level'];
    }



}
