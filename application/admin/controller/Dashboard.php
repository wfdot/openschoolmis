<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Config;

/**
 * 控制台
 *
 * @icon fa fa-dashboard
 * @remark 用于展示当前系统中的统计数据、统计报表及重要实时数据
 */
class Dashboard extends Backend
{

    /**
     * 查看
     */
    public function index()
    {
        $seventtime = \fast\Date::unixtime('day', -7);
        $paylist = $createlist = [];
        for ($i = 0; $i < 7; $i++)
        {
            $day = date("Y-m-d", $seventtime + ($i * 86400));
            $createlist[$day] = mt_rand(20, 200);
            $paylist[$day] = mt_rand(1, mt_rand(1, $createlist[$day]));
        }
        $hooks = config('addons.hooks');
        $uploadmode = isset($hooks['upload_config_init']) && $hooks['upload_config_init'] ? implode(',', $hooks['upload_config_init']) : 'local';
        $addonComposerCfg = ROOT_PATH . '/vendor/karsonzhang/fastadmin-addons/composer.json';
        Config::parse($addonComposerCfg, "json", "composer");
        $config = Config::get("composer");
        $addonVersion = isset($config['version']) ? $config['version'] : __('Unknown');

        //获取后台数据1、总学生数。
        //1、总学生数。
        $this->stu = new \app\admin\model\school\Student();
        $totalStudent = $this->stu->getTotalNum();
        //2、总课程数
        $this->lesson = new \app\admin\model\school\Lesson();
        $totalLesson = $this->lesson->getTotalNum();
        //3、总专业数
        $this->major = new \app\admin\model\school\Major();
        $totalMajor = $this->major->getTotalNum();
        //4、总教师数
        $this->teacher = new \app\admin\model\Admin();
        $totalTeacher = $this->teacher->getTotalNum();
        $this->view->assign([
            'totaluser'        => $totalStudent,
            'totalviews'       => $totalLesson,
            'totalorder'       => $totalMajor,
            'totalorderamount' => $totalTeacher,
            'todayuserlogin'   => 321,
            'todayusersignup'  => 430,
            'todayorder'       => 2324,
            'unsettleorder'    => 132,
            'sevendnu'         => '80%',
            'sevendau'         => '32%',
            'paylist'          => $paylist,
            'createlist'       => $createlist,
            'addonversion'       => $addonVersion,
            'uploadmode'       => $uploadmode
        ]);

        return $this->view->fetch("index");
    }

}
