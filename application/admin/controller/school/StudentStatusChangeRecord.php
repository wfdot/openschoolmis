<?php

namespace app\admin\controller\school;

use app\common\controller\Backend;

/**
 * 学籍异动管理
 *
 * @icon fa fa-circle-o
 */
class StudentStatusChangeRecord extends Backend
{
    
    /**
     * StudentStatusChangeRecord模型对象
     * @var \app\admin\model\school\StudentStatusChangeRecord
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\school\StudentStatusChangeRecord;
        $this->view->assign("studentSexList", $this->model->getStudentSexList());
        $this->view->assign("majorLevelDictList", $this->model->getMajorLevelDictList());
        $this->assignconfig("majorLevelDictList", $this->model->getMajorLevelDictList());
        $this->view->assign("beforeStatusList", $this->model->getBeforeStatusList());
        $this->view->assign("afterStatusList", $this->model->getAfterStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams(null,true);
            // $newSort = [];
            // foreach (explode(',', $sort) as $item) {
            //     $newSort[] = 'f.' . $item;
            // }
            // $sort = implode($newSort);
            $total = $this->model->alias("f")->join("student s", "s.id = f.student_id")
                ->where($where)
                ->order($sort, $order)
//                ->buildSql();
                ->count();



            $list = $this->model
                ->field("f.*")
                ->alias("f")->join("student s", "s.id = f.student_id")
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
//                ->buildSql();
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
}
