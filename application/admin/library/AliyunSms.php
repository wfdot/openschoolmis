<?php


namespace app\admin\library;


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

class AliyunSms
{
    public function __construct()
    {
        AlibabaCloud::accessKeyClient('LTAIGg5HLr4jr3jb', '7jYLWfYZvLX4RZyyIMhChXtdshhPCT')
            ->regionId('cn-hangzhou')
            ->asDefaultClient();
    }

    public function smsSend(&$params)
    {
        switch ($params['event']) {
            case "restPwdAdmin":
                $TemplateCode = "SMS_159370045";
                break;
            case "loginInAdmin":
            case "loginInStudent":
                $TemplateCode = "SMS_159370048";
                break;
            case "registerAdmin":
                $TemplateCode = "SMS_159370046";
                break;
            default:
                $TemplateCode = "SMS_159370045";
        }
        $code = $params['code'];

        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'PhoneNumbers' => $params['mobile'],
                        'SignName' => "金美通信",
                        'TemplateCode' => $TemplateCode,
                        'TemplateParam' => "{code:\"{$code}\"}",
                    ],
                ])
                ->request();

            $result = $result->toArray();

            if ($result['Code'] == "OK") {
                return true;
            }
//            print_r($toArray);
        } catch (ClientException $e) {
//            echo $e->getErrorMessage() . PHP_EOL;
            return false;
        } catch (ServerException $e) {
//            echo $e->getErrorMessage() . PHP_EOL;
            return false;
        }
    }
    public function smsCheck(&$params)
    {
        return TRUE;
    }
}