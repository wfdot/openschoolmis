<?php

return [
    'Student_id'       => '学号',
    'Student_name'     => '姓名',
    'Student_sex'      => '性别',
    'Student_sex 0'    => '男',
    'Student_sex 1'    => '女',
    'Major_level_dict' => '培养层次',
    'Student_level'    => '级别',
    'Before_status'    => '变动前状态',
    'Before_status 0'  => '在读',
    'Before_status 1'  => '休学',
    'Before_status 2'  => '毕业',
    'Before_status 3'  => '退学',
    'After_status'     => '变动后状态',
    'After_status 0'   => '在读',
    'After_status 1'   => '休学',
    'After_status 2'   => '毕业',
    'After_status 3'   => '退学',
    'Createtime'       => '创建时间',
    'Updatetime'       => '更新时间',
    'Admin_id'         => '操作人'
];
