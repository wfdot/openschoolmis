<?php

return [
    'Id'               => 'id',
    'Student_id'       => '学号',
    'Student_name'     => '姓名',
    'Major_id'         => '专业',
    'Major_level_dict' => '培养层次',
    'Level'            => '级别',
    'Term_dict'        => '学期',
    'Sex'              => '性别',
    'Sex 0'            => '男',
    'Sex 1'            => '女',
    'Status'           => '学员状态',
    'Status 0'         => '未缴',
    'Status 1'         => '已缴',
    'Money'            => '缴纳费用',
    'Remark'           => '备注',
    'Createtime'       => '创建时间',
    'Updatetime'       => '更新时间',
    'Deletetime'       => '删除时间'
];
