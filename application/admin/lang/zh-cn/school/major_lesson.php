<?php

return [
    'Id'               => 'id',
    'Major_id'         => '专业id',
    'Lesson_id'        => '课程id',
    'Major_level_dict' => '培养层次',
    'Level'            => '级别',
    'Admin_ids'        => '任课老师',
    'Class_time_dict'  => '上课时间',
    'Class_date'       => '上课日期',
    'Room_dict'        => '教室',
    'Createtime'       => '创建时间',
    'Updatetime'       => '更新时间',
    'Remark'           => '备注信息',
    'Deletetime'       => '删除时间'
];
